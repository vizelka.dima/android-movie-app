package cz.cvut.fit.and.vizeldim.movieapp.features.movie.presentation.adapter.movie

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.core.view.isVisible
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import cz.cvut.fit.and.vizeldim.movieapp.R
import cz.cvut.fit.and.vizeldim.movieapp.databinding.ItemMovieBinding
import cz.cvut.fit.and.vizeldim.movieapp.features.movie.domain.Movie
import cz.cvut.fit.and.vizeldim.movieapp.features.movie.presentation.component.RatingComponent


class MovieAdapter(
    private val bindingStrategy: ItemMovieBindingStrategy = ItemMovieNoBindingStrategy(),
    private val onClick: (Movie) -> Unit
) : ListAdapter<Movie, MovieAdapter.MovieHolder>(MovieDiffCallback()) {

    init {
        // Correctly restores scroll position on screen rotation.
        stateRestorationPolicy = StateRestorationPolicy.PREVENT_WHEN_EMPTY
    }

    override fun onCreateViewHolder(viewGroup: ViewGroup, viewType: Int): MovieHolder {
        val binding = ItemMovieBinding.inflate(
            LayoutInflater.from(viewGroup.context),
            viewGroup,
            false
        )
        return MovieHolder(binding, bindingStrategy, onClick)
    }

    override fun onBindViewHolder(movieHolder: MovieHolder, position: Int) {
        val movie = getItem(position)
        movieHolder.bind(movie)
    }

    class MovieHolder(
        private val binding: ItemMovieBinding,
        private val bindingStrategy: ItemMovieBindingStrategy,
        private val onClick: (Movie) -> Unit
    ) :
        RecyclerView.ViewHolder(binding.root) {

        private fun bindImg(imgUrl: String) {
            Glide.with(binding.root).load("https://image.tmdb.org/t/p/w200${imgUrl}")
                .into(binding.img)

        }

        fun bind(movie: Movie) {
            movie.posterImg?.let { bindImg(it) }
            binding.txtTitle.text = movie.title
            binding.root.setOnClickListener { onClick(movie) }

            bindingStrategy.bind(binding, movie)
        }
    }

    class MovieDiffCallback : DiffUtil.ItemCallback<Movie>() {
        override fun areItemsTheSame(
            oldItem: Movie,
            newItem: Movie
        ): Boolean {
            return oldItem.id == newItem.id
        }

        override fun areContentsTheSame(
            oldItem: Movie,
            newItem: Movie
        ): Boolean {
            return oldItem == newItem
        }
    }


    interface ItemMovieBindingStrategy {
        fun bind(binding: ItemMovieBinding, movie: Movie)
    }

    class ItemMovieNoBindingStrategy : ItemMovieBindingStrategy {
        override fun bind(binding: ItemMovieBinding, movie: Movie) {
            binding.txtDate.isVisible = false
            binding.rating.root.isVisible = false
        }
    }

    class ItemMovieRatingBindingStrategy : ItemMovieBindingStrategy {
        override fun bind(binding: ItemMovieBinding, movie: Movie) {
            binding.txtDate.isVisible = false

            val rating = RatingComponent(
                binding = binding.rating,
                ratingResourceActive = R.drawable.ic_star_filled_small,
                ratingResourceDisabled = R.drawable.ic_star_filled_inactive_small
            )
            rating.render(movie.avgVotes)
        }
    }

    class ItemMovieDateBindingStrategy : ItemMovieBindingStrategy {
        override fun bind(binding: ItemMovieBinding, movie: Movie) {
            binding.rating.root.isVisible = false
            binding.txtDate.text = movie.releaseDate
        }
    }

}
