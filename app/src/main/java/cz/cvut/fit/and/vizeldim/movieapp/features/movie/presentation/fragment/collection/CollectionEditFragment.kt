package cz.cvut.fit.and.vizeldim.movieapp.features.movie.presentation.fragment.collection

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.lifecycle.LifecycleOwner
import androidx.navigation.fragment.navArgs
import androidx.viewpager2.adapter.FragmentStateAdapter
import com.google.android.material.dialog.MaterialAlertDialogBuilder
import com.google.android.material.tabs.TabLayoutMediator
import cz.cvut.fit.and.vizeldim.movieapp.databinding.FragmentEditCollectionBinding
import cz.cvut.fit.and.vizeldim.movieapp.features.movie.domain.Movie
import cz.cvut.fit.and.vizeldim.movieapp.features.movie.domain.SimpleMovie
import cz.cvut.fit.and.vizeldim.movieapp.features.movie.presentation.adapter.movie.SearchMovieAdapter
import cz.cvut.fit.and.vizeldim.movieapp.features.movie.presentation.adapter.movie.SimpleMovieAdapter
import cz.cvut.fit.and.vizeldim.movieapp.features.movie.presentation.fragment.movie.BaseMovieSearchFragment
import cz.cvut.fit.and.vizeldim.movieapp.features.movie.presentation.viewmodel.collection.CollectionDetailViewModel
import org.koin.androidx.viewmodel.ext.android.viewModel


class CollectionEditFragment: Fragment() {
    private var _binding: FragmentEditCollectionBinding? = null

    private val binding: FragmentEditCollectionBinding
        get() = _binding!!

    private val args: CollectionEditFragmentArgs by navArgs()

    private var tabs: Map<CollectionEditTab, Fragment> = emptyMap()

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        _binding = FragmentEditCollectionBinding.inflate(inflater)

        initPager()
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        TabLayoutMediator(binding.tabLayout, binding.pager) { tab, position ->
            if(position >= tabs.keys.size) {
                return@TabLayoutMediator
            }
            val tabTitles = tabs.keys.toList()
            tab.text = tabTitles[position].title
        }.attach()

    }


    private fun initPager() {
        val removeTab = CollectionRemoveMovieFragment()
        removeTab.arguments = Bundle().apply {
            putLong("collectionId", args.collectionId)
            putString("collectionName", args.collectionName)
        }

        val addTab = CollectionAddMovieFragment()
        addTab.arguments = Bundle().apply {
            putLong("collectionId", args.collectionId)
            putString("collectionName", args.collectionName)
        }


        tabs = mapOf(
            CollectionEditTab.ADD to addTab,
            CollectionEditTab.REMOVE to removeTab
        )


        val p = binding.pager
        p.adapter = CollectionEditPagerAdapter(this, tabs.values.toList())
    }


    enum class CollectionEditTab(val title: String) {
        ADD("Add"), REMOVE("Remove")
    }

}

class CollectionEditPagerAdapter(fragment: Fragment,
                                 private val tabs: List<Fragment>) : FragmentStateAdapter(fragment) {

    override fun getItemCount(): Int = tabs.size

    override fun createFragment(position: Int): Fragment = tabs[position]
}


class CollectionAddMovieFragment : BaseMovieSearchFragment() {
    override val adapter = SearchMovieAdapter(::showAddDialog)

    override val myViewLifecycleOwner: LifecycleOwner
        get() = viewLifecycleOwner

    private val collectionViewModel by viewModel<CollectionDetailViewModel>()

    private val args: CollectionAddMovieFragmentArgs by navArgs()

    private val confirmDialog by lazy {
        MaterialAlertDialogBuilder(requireContext())
            .setTitle("Confirm add action")
            .setNegativeButton("Cancel") { dialog, _ ->
                dialog.dismiss()
            }
    }

    override fun afterBinding() {
        collectionViewModel.loadMovies(args.collectionId)
    }

    override fun additionalViewModelObserver() {
        collectionViewModel.collectionDetailStateStream.observe(myViewLifecycleOwner) {
            state ->
                if(state != null) {
                    if(state.successAdd && state.movies.isNotEmpty()) {
                        handleSuccessAdd()
                    } else if(state.notEnoughSpace) {
                        handleNotEnoughSpace()
                    }
                }
        }
    }

    private fun handleNotEnoughSpace() {
        collectionViewModel.hideSuccessNotEnoughSpace()

        Toast.makeText(context, "Collection can contain max. 20 movies", Toast.LENGTH_SHORT).show()
    }

    private fun handleSuccessAdd() {
        collectionViewModel.hideSuccessAdd()

        Toast.makeText(context, "Added to collection", Toast.LENGTH_SHORT).show()
        viewModel.searchStateStream.value?.let { search(it.searchName) }
    }

    override fun search(query: String?) {
        query?.let {
            val collectionMovies = collectionViewModel.collectionDetailStateStream.value?.movies
            if(collectionMovies == null) { viewModel.searchByName(it) }
            else {
                viewModel.searchByName(it) {
                    movie -> (collectionMovies.find { x -> x.id == movie.id.toLong() } == null)
                }
            }
        }
    }


    private fun addAction(movie: Movie) {
        val simpleMovie = SimpleMovie(movie.id.toLong(), movie.title, movie.posterImg)
        collectionViewModel.addMovie(args.collectionId, simpleMovie)
    }

    private fun showAddDialog(movie: Movie) {
        confirmDialog
            .setMessage("Add movie '${movie.title}' to collection '${args.collectionName}'?")
            .setPositiveButton("Add") { dialog, _ ->
                addAction(movie)
                dialog.dismiss()
            }
            .show()
    }
}


class CollectionRemoveMovieFragment : BaseCollectionDetailFragment() {
    override val adapter = SimpleMovieAdapter(::showRemoveDialog)
 //resources.getString(R.string.title)
    private val confirmDialog by lazy {
        MaterialAlertDialogBuilder(requireContext())
            .setTitle("Confirm remove action")
            .setNegativeButton("Cancel") { dialog, _ ->
                dialog.dismiss()
            }
    }

    override fun createViewModelObserver() {
        viewModel.collectionDetailStateStream.observe(viewLifecycleOwner) {
                state -> state?.apply {
                    if(state.successRemove) {
                        handleSuccessRemove()
                    }
                    adapter.submitList(movies)
                }
        }

    }

    private fun handleSuccessRemove() {
        Toast.makeText(context, "Removed from collection", Toast.LENGTH_SHORT).show()
        viewModel.hideSuccessRemove()
    }
    private fun removeAction(movie: SimpleMovie) {
        viewModel.removeMovie(args.collectionId, movie)
    }

    private fun showRemoveDialog(movie: SimpleMovie) {
        confirmDialog
            .setMessage("Remove movie '${movie.title}' from collection '${args.collectionName}'?")
            .setPositiveButton("Remove") { dialog, _ ->
                removeAction(movie)
                dialog.dismiss()
            }
            .show()
    }
}
