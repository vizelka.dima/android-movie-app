package cz.cvut.fit.and.vizeldim.movieapp.features.movie.data.retrofit

import cz.cvut.fit.and.vizeldim.movieapp.features.movie.domain.Genre
import cz.cvut.fit.and.vizeldim.movieapp.features.movie.domain.Movie
import cz.cvut.fit.and.vizeldim.movieapp.features.movie.domain.Person
import cz.cvut.fit.and.vizeldim.movieapp.features.movie.domain.Video

fun ApiMovie.toDomain(): Movie {
    return Movie(
        id,
        title,
        detailImg,
        posterImg,
        avgVotes,
        runtime,
        releaseDate,
        genres?.map { it.toDomain() } ?: emptyList(),
        videos?.results?.firstOrNull()?.toDomain(),
        credits?.cast?.map { it.toDomain() } ?: emptyList(),
    )
}

fun ApiPerson.toDomain(): Person {
    return Person(id.toLong(), name, img, knownForDepartment)
}

private fun ApiGenre.toDomain(): Genre {
    return Genre(id, name)
}

private fun ApiVideo.toDomain(): Video {
    return Video(site, key)
}


fun ApiMovieListResponse.toDomain(): List<Movie> {
    return results.toDomain()
}

fun ApiPersonListResponse.toDomain(): List<Person> {
    return results.toDomain()
}


@JvmName("toDomainApiPerson")
fun List<ApiPerson>.toDomain(): List<Person> {
    return map { it.toDomain() }
}

fun List<ApiMovie>.toDomain(): List<Movie> {
    return map { it.toDomain() }
}




