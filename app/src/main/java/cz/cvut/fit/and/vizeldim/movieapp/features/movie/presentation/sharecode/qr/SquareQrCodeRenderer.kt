package cz.cvut.fit.and.vizeldim.movieapp.features.movie.presentation.sharecode.qr

import android.graphics.Bitmap
import android.graphics.Color
import cz.cvut.fit.and.vizeldim.movieapp.features.movie.presentation.sharecode.CodeRenderer
import cz.cvut.fit.and.vizeldim.movieapp.features.movie.sharecode.CodeInterface
import kotlin.coroutines.resume
import kotlin.coroutines.suspendCoroutine


class SquareQrCodeRenderer: CodeRenderer {
    override suspend fun render(rawData: CodeInterface) : Bitmap =
        suspendCoroutine { c ->
            val bmp: Bitmap = Bitmap.createBitmap(rawData.width, rawData.height, Bitmap.Config.RGB_565)

            for(x in 0 until rawData.width) {
                for(y in 0 until rawData.height) {
                    bmp.setPixel(x, y, if(rawData.get(x, y)) Color.BLACK else Color.WHITE)
                }
            }

            c.resume(bmp)
        }
}