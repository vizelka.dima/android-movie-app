package cz.cvut.fit.and.vizeldim.movieapp.features.movie.sharecode.qr

import com.google.zxing.BarcodeFormat
import com.google.zxing.MultiFormatWriter
import com.google.zxing.WriterException
import com.google.zxing.common.BitMatrix
import cz.cvut.fit.and.vizeldim.movieapp.features.movie.sharecode.CodeGeneratorException
import cz.cvut.fit.and.vizeldim.movieapp.features.movie.sharecode.CodeGeneratorInterface
import cz.cvut.fit.and.vizeldim.movieapp.features.movie.sharecode.CodeInterface
import kotlin.coroutines.resume
import kotlin.coroutines.resumeWithException
import kotlin.coroutines.suspendCoroutine

class ZxingQrCodeGenerator : CodeGeneratorInterface {
    override suspend fun generate(codeContentStr: String): CodeInterface =
        suspendCoroutine { c ->
            val qrWriter = MultiFormatWriter()

            try {
                val bitMatrix: BitMatrix =
                    qrWriter.encode(codeContentStr, BarcodeFormat.QR_CODE, 1000, 1000)

                val res = ZxingQrCode(bitMatrix)

                c.resume(res)
            } catch (e: WriterException) {
                c.resumeWithException(CodeGeneratorException())
            }
        }
}

