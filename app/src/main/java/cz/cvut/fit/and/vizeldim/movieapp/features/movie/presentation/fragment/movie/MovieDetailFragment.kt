package cz.cvut.fit.and.vizeldim.movieapp.features.movie.presentation.fragment.movie

import android.content.res.Configuration
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.os.bundleOf
import androidx.core.view.isVisible
import androidx.fragment.app.Fragment
import androidx.navigation.fragment.findNavController
import androidx.navigation.fragment.navArgs
import androidx.recyclerview.widget.LinearLayoutManager
import com.bumptech.glide.Glide
import com.google.android.material.snackbar.Snackbar
import com.google.firebase.analytics.FirebaseAnalytics
import cz.cvut.fit.and.vizeldim.movieapp.R
import cz.cvut.fit.and.vizeldim.movieapp.databinding.FragmentMovieDetailBinding
import cz.cvut.fit.and.vizeldim.movieapp.features.movie.domain.Movie
import cz.cvut.fit.and.vizeldim.movieapp.features.movie.domain.Person
import cz.cvut.fit.and.vizeldim.movieapp.features.movie.presentation.adapter.genre.GenreAdapter
import cz.cvut.fit.and.vizeldim.movieapp.features.movie.presentation.adapter.person.PersonAdapter
import cz.cvut.fit.and.vizeldim.movieapp.features.movie.presentation.component.RatingComponent
import cz.cvut.fit.and.vizeldim.movieapp.features.movie.presentation.component.YoutubePlayerComponent
import cz.cvut.fit.and.vizeldim.movieapp.features.movie.presentation.viewmodel.movie.DetailViewModel
import org.koin.androidx.viewmodel.ext.android.viewModel


class MovieDetailFragment : Fragment() {
    private val args: MovieDetailFragmentArgs by navArgs()

    private var _binding: FragmentMovieDetailBinding? = null
    private val binding get() = _binding!!
    private lateinit var mFirebaseAnalytics: FirebaseAnalytics

    private val adapter = PersonAdapter(::navigateToPerson)
    private val genreAdapter = GenreAdapter()

    private val viewModel by viewModel<DetailViewModel>()

    private lateinit var youtubePlayerComponent: YoutubePlayerComponent

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setHasOptionsMenu(true)
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {

        _binding = FragmentMovieDetailBinding.inflate(inflater, container, false)

        val youTubePlayerView = binding.youtubePlayer
        youtubePlayerComponent = YoutubePlayerComponent(youTubePlayerView)
        lifecycle.addObserver(youTubePlayerView)

        return binding.root
    }


    override fun onConfigurationChanged(newConfig: Configuration) {
        super.onConfigurationChanged(newConfig)

        // Checks the orientation of the screen
        if (newConfig.orientation == Configuration.ORIENTATION_LANDSCAPE) {
            youtubePlayerComponent.enterFullScreen()
        } else if (newConfig.orientation == Configuration.ORIENTATION_PORTRAIT) {
            youtubePlayerComponent.exitFullScreen()
        }
    }


    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        binding.sectionCast.imageView.isVisible = false

        binding.sectionCast.sectionAction.isVisible = false
        viewModel.getById(args.movieId)

        loadMovie()

        mFirebaseAnalytics = FirebaseAnalytics.getInstance(requireContext())

        mFirebaseAnalytics.logEvent(
            "movie_detail_opened",
            bundleOf(
                "movie_id" to args.movieId,
                "movie_title" to args.movieTitle
            )
        )
    }

    private fun loadMovie() {
        viewModel.detailStateStream.observe(viewLifecycleOwner) { state ->
            if (state != null) {
                binding.loader.isVisible = state.isLoading

                if (state.showError) {
                    handleError()
                } else {
                    adapter.submitList(state.movieDetail?.casts)
                    genreAdapter.submitList(state.movieDetail?.genres)

                    state.movieDetail?.let { binding.bind(it) }
                }
            }
        }
    }

    private fun handleError() {
        Snackbar.make(binding.root, "Error loading", Snackbar.LENGTH_SHORT).show()
        viewModel.hideError()
    }

    private fun navigateToPerson(person: Person) {
        val directions =
            MovieDetailFragmentDirections.actionToPersonMovies(
                person.id,
                person.name
            )
        findNavController().navigate(directions)
    }


    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }


    private fun FragmentMovieDetailBinding.bind(movie: Movie) {
        val ratingComponent = RatingComponent(
            rating,
            R.drawable.ic_star_filled,
            R.drawable.ic_star_filled_inactive
        )
        ratingComponent.render(movie.avgVotes)

        genres.adapter = genreAdapter
        genres.layoutManager = LinearLayoutManager(context, LinearLayoutManager.HORIZONTAL, false)

        bindVisuals(movie)

        txtTitle.text = movie.title
        txtTime.text = movie.runtimeStr
        txtYear.text = movie.releaseDate

        binding.sectionCast.list.layoutManager =
            LinearLayoutManager(context, LinearLayoutManager.HORIZONTAL, false)
        binding.sectionCast.list.adapter = adapter
        binding.sectionCast.sectionTitle.text = "Cast"
    }

    private fun FragmentMovieDetailBinding.bindVisuals(movie: Movie) {
        if (movie.video == null) {
            youtubePlayer.isVisible = false

            val img = movie.detailImg ?: movie.posterImg

            img?.let {
                Glide.with(root).load("https://image.tmdb.org/t/p/w400${it}")
                    .into(binding.imgMovieDetail)
            }

        } else {
            imgMovieDetail.isVisible = false
            youtubePlayerComponent.cueVideo(movie.video.key)
        }
    }
}


