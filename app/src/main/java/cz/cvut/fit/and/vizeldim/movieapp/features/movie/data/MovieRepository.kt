package cz.cvut.fit.and.vizeldim.movieapp.features.movie.data

import cz.cvut.fit.and.vizeldim.movieapp.features.movie.domain.Movie
import cz.cvut.fit.and.vizeldim.movieapp.features.movie.domain.MovieCollection
import cz.cvut.fit.and.vizeldim.movieapp.features.movie.domain.Person
import cz.cvut.fit.and.vizeldim.movieapp.features.movie.domain.SimpleMovie
import kotlinx.coroutines.flow.Flow


class MovieRepository(
    private val movieRemoteDataSource: MovieRemoteDataSource,
    private val movieCollectionDbDataSource: MovieCollectionDbDataSource
) : IMovieRepository {
    override suspend fun getPopularPage(page: Int): List<Movie> {
        return movieRemoteDataSource.getPopularPage(page)
    }

    override suspend fun getTopRatedPage(page: Int): List<Movie> {
        return movieRemoteDataSource.getTopRatedPage(page)
    }

    override suspend fun getUpcomingPage(page: Int): List<Movie> {
        return movieRemoteDataSource.getUpcomingPage(page)
    }

    override suspend fun getPageByName(
        name: String,
        page: Int
    ): List<Movie> {
        return movieRemoteDataSource.getPageByName(name, page)
    }

    override suspend fun getById(id: Long): Movie {
        return movieRemoteDataSource.getById(id)
    }

    override suspend fun getPopularPersonPage(page: Int): List<Person> {
        return movieRemoteDataSource.getPopularPersonPage(page)
    }

    override suspend fun getPersonPageByName(
        name: String,
        page: Int
    ): List<Person> {
        return movieRemoteDataSource.getPersonPageByName(name, page)
    }

    override suspend fun getPageByPersonId(
        id: Long,
        page: Int
    ): List<Movie> {
        return movieRemoteDataSource.getPageByPersonId(id, page)
    }

    override fun streamCollections(): Flow<List<MovieCollection>> =
        movieCollectionDbDataSource.streamAll()

    override suspend fun addCollection(name: String): Long {
        return movieCollectionDbDataSource.create(name)
    }

    override suspend fun addCollection(name: String, movies: List<SimpleMovie>): Long {
        return movieCollectionDbDataSource.create(name, movies)
    }

    override fun streamMoviesByCollectionId(collectionId: Long): Flow<List<SimpleMovie>> =
        movieCollectionDbDataSource.streamMovies(collectionId)


    override suspend fun addMovieToCollection(collectionId: Long, movie: SimpleMovie): Long {
        return movieCollectionDbDataSource.addMovie(collectionId, movie)
    }

    override suspend fun removeMovieFromCollection(
        collectionId: Long,
        movie: SimpleMovie
    ): Boolean {
        return movieCollectionDbDataSource.removeMovie(collectionId, movie)
    }

    override suspend fun updateCollectionName(collectionId: Long, name: String): Boolean {
        return movieCollectionDbDataSource.updateCollectionName(collectionId, name)
    }

    override fun streamCollectionById(collectionId: Long): Flow<MovieCollection> {
        return movieCollectionDbDataSource.streamCollectionById(collectionId)
    }
}