package cz.cvut.fit.and.vizeldim.movieapp.features.movie.presentation.adapter.person

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import cz.cvut.fit.and.vizeldim.movieapp.R
import cz.cvut.fit.and.vizeldim.movieapp.databinding.ItemPersonBinding
import cz.cvut.fit.and.vizeldim.movieapp.features.movie.domain.Person


class PersonAdapter(
    private val onPersonClick: (Person) -> Unit
) : ListAdapter<Person, PersonAdapter.PersonHolder>(PersonDiffCallback()) {

    init {
        // Correctly restores scroll position on screen rotation.
        stateRestorationPolicy = StateRestorationPolicy.PREVENT_WHEN_EMPTY
    }

    override fun onCreateViewHolder(viewGroup: ViewGroup, viewType: Int): PersonHolder {
        val binding = ItemPersonBinding.inflate(
            LayoutInflater.from(viewGroup.context),
            viewGroup,
            false
        )
        return PersonHolder(binding)
    }

    override fun onBindViewHolder(movieHolder: PersonHolder, position: Int) {
        val person = getItem(position)
        movieHolder.bind(person, onPersonClick)
    }

    class PersonHolder(private val binding: ItemPersonBinding) :
        RecyclerView.ViewHolder(binding.root) {

        fun bind(
            person: Person,
            onPersonClick: (Person) -> Unit
        ) {
            if (person.img != null) {
                person.img.let {
                    Glide.with(itemView).load("https://image.tmdb.org/t/p/w200${it}")
                        .into(binding.imgAvatar)
                }
            } else {
                binding.imgAvatar.setImageResource(R.drawable.avatar_blank_2)
            }

            binding.txtName.text = person.name
            binding.root.setOnClickListener { onPersonClick(person) }
        }
    }

    class PersonDiffCallback : DiffUtil.ItemCallback<Person>() {

        override fun areItemsTheSame(
            oldItem: Person,
            newItem: Person
        ): Boolean {
            return oldItem.id == newItem.id
        }

        override fun areContentsTheSame(
            oldItem: Person,
            newItem: Person
        ): Boolean {
            return oldItem == newItem
        }
    }
}
