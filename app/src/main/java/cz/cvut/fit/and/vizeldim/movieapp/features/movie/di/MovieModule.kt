package cz.cvut.fit.and.vizeldim.movieapp.features.movie.di

import cz.cvut.fit.and.vizeldim.movieapp.features.movie.data.IMovieRepository
import cz.cvut.fit.and.vizeldim.movieapp.features.movie.data.MovieCollectionDbDataSource
import cz.cvut.fit.and.vizeldim.movieapp.features.movie.data.MovieRemoteDataSource
import cz.cvut.fit.and.vizeldim.movieapp.features.movie.data.MovieRepository
import cz.cvut.fit.and.vizeldim.movieapp.features.movie.data.paging.person.PersonPagingSource
import cz.cvut.fit.and.vizeldim.movieapp.features.movie.data.retrofit.MovieApiDescription
import cz.cvut.fit.and.vizeldim.movieapp.features.movie.data.retrofit.MovieRetrofitDataSource
import cz.cvut.fit.and.vizeldim.movieapp.features.movie.data.room.MovieCollectionRoomDataSource
import cz.cvut.fit.and.vizeldim.movieapp.features.movie.presentation.viewmodel.collection.CollectionDetailViewModel
import cz.cvut.fit.and.vizeldim.movieapp.features.movie.presentation.viewmodel.collection.CollectionImportViewModel
import cz.cvut.fit.and.vizeldim.movieapp.features.movie.presentation.viewmodel.collection.CollectionShareViewModel
import cz.cvut.fit.and.vizeldim.movieapp.features.movie.presentation.viewmodel.collection.CollectionsViewModel
import cz.cvut.fit.and.vizeldim.movieapp.features.movie.presentation.viewmodel.movie.DetailViewModel
import cz.cvut.fit.and.vizeldim.movieapp.features.movie.presentation.viewmodel.movie.MovieListViewModel
import cz.cvut.fit.and.vizeldim.movieapp.features.movie.presentation.viewmodel.movie.MovieViewModel
import cz.cvut.fit.and.vizeldim.movieapp.features.movie.presentation.viewmodel.movie.SearchMovieViewModel
import cz.cvut.fit.and.vizeldim.movieapp.features.movie.presentation.viewmodel.person.PeopleViewModel
import cz.cvut.fit.and.vizeldim.movieapp.features.movie.presentation.viewmodel.person.SearchPeopleViewModel
import cz.cvut.fit.and.vizeldim.movieapp.features.movie.sharecode.CodeContentParserInterface
import cz.cvut.fit.and.vizeldim.movieapp.features.movie.sharecode.qr.JsonCodeContentParser
import cz.cvut.fit.and.vizeldim.movieapp.shared.data.room.MovieCollectionDatabase
import org.koin.androidx.viewmodel.dsl.viewModel
import org.koin.dsl.module
import retrofit2.Retrofit

val movieModule = module {
    factory {
        get<Retrofit>().create(MovieApiDescription::class.java)
    }

    factory<MovieRemoteDataSource> {
        MovieRetrofitDataSource(movieApiDescription = get())
    }

    factory {
        PersonPagingSource(repository = get())
    }

    factory {
        get<MovieCollectionDatabase>().movieCollectionDao()
    }

    factory<MovieCollectionDbDataSource> {
        MovieCollectionRoomDataSource(dao = get())
    }

    single<IMovieRepository> {
        MovieRepository(movieRemoteDataSource = get(), movieCollectionDbDataSource = get())
    }


    single<CodeContentParserInterface> {
        JsonCodeContentParser()
    }

    viewModel {
        MovieViewModel(movieRepository = get())
    }

    viewModel {
        SearchMovieViewModel(movieRepository = get())
    }

    viewModel {
        DetailViewModel(movieRepository = get())
    }


    viewModel {
        PeopleViewModel(personPagingSource = get())
    }

    viewModel {
        SearchPeopleViewModel(movieRepository = get())
    }

    viewModel {
        MovieListViewModel(repository = get())
    }

    viewModel {
        CollectionsViewModel(repository = get())
    }

    viewModel {
        CollectionDetailViewModel(repository = get())
    }


    viewModel {
        CollectionShareViewModel(repository = get(), codeParser = get())
    }

    viewModel {
        CollectionImportViewModel(repository = get(), codeParser = get())
    }
}