package cz.cvut.fit.and.vizeldim.movieapp.features.movie.domain

data class Video(
    val site: String,
    val key: String
)
