package cz.cvut.fit.and.vizeldim.movieapp.features.movie.sharecode.qr

import android.content.Context
import com.budiyev.android.codescanner.*
import cz.cvut.fit.and.vizeldim.movieapp.features.movie.sharecode.CodeScannerInterface
import kotlin.coroutines.resume
import kotlin.coroutines.resumeWithException
import kotlin.coroutines.suspendCoroutine

class BudiyevCodeScanner(
    context: Context,
    codeScannerView: CodeScannerView
) : CodeScannerInterface {

    private val codeScanner = CodeScanner(context, codeScannerView)

    init {
        codeScanner.camera = CodeScanner.CAMERA_BACK
        codeScanner.formats = CodeScanner.ALL_FORMATS

        codeScanner.autoFocusMode = AutoFocusMode.SAFE
        codeScanner.scanMode = ScanMode.SINGLE
        codeScanner.isAutoFocusEnabled = true
        codeScanner.isFlashEnabled = false

        codeScannerView.setOnClickListener {
            codeScanner.startPreview()
        }
    }

    override suspend fun scan(): String {
        codeScanner.startPreview()

        return suspendCoroutine { c ->
            codeScanner.decodeCallback = DecodeCallback {
                c.resume(it.text)
            }

            codeScanner.errorCallback = ErrorCallback {
                c.resumeWithException(it)
            }
        }
    }
}