package cz.cvut.fit.and.vizeldim.movieapp.features.movie.presentation.fragment.movie

import android.os.Bundle
import android.view.*
import android.widget.Toast
import androidx.core.view.isVisible
import androidx.fragment.app.Fragment
import androidx.lifecycle.LiveData
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import cz.cvut.fit.and.vizeldim.movieapp.R
import cz.cvut.fit.and.vizeldim.movieapp.databinding.FragmentMainBinding
import cz.cvut.fit.and.vizeldim.movieapp.databinding.SectionSliderHorizontalBinding
import cz.cvut.fit.and.vizeldim.movieapp.features.movie.data.paging.movie.MoviePagingStrategy
import cz.cvut.fit.and.vizeldim.movieapp.features.movie.data.paging.movie.PopularMoviePagingStrategy
import cz.cvut.fit.and.vizeldim.movieapp.features.movie.data.paging.movie.TopRatedMoviePagingStrategy
import cz.cvut.fit.and.vizeldim.movieapp.features.movie.data.paging.movie.UpcomingMoviePagingStrategy
import cz.cvut.fit.and.vizeldim.movieapp.features.movie.domain.Movie
import cz.cvut.fit.and.vizeldim.movieapp.features.movie.presentation.adapter.movie.MovieAdapter
import cz.cvut.fit.and.vizeldim.movieapp.features.movie.presentation.viewmodel.movie.MovieState
import cz.cvut.fit.and.vizeldim.movieapp.features.movie.presentation.viewmodel.movie.MovieViewModel
import org.koin.androidx.viewmodel.ext.android.viewModel

class MainFragment : Fragment() {
    private var _binding: FragmentMainBinding? = null

    // This property is only valid between onCreateView and
    // onDestroyView.
    private val binding get() = _binding!!

    private val viewModel by viewModel<MovieViewModel>()

    private val popularAdapter = MovieAdapter(
        bindingStrategy = MovieAdapter.ItemMovieRatingBindingStrategy(),
        onClick = ::navigateToDetail
    )

    private val upcomingAdapter = MovieAdapter(
        bindingStrategy = MovieAdapter.ItemMovieDateBindingStrategy(),
        onClick = ::navigateToDetail
    )

    private val topRatedAdapter = MovieAdapter(onClick = ::navigateToDetail)

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setHasOptionsMenu(true)
    }


    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        _binding = FragmentMainBinding.inflate(inflater, container, false)
        return binding.root

    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        binding.sectionPopular.bind(
            "Popular",
            PopularMoviePagingStrategy(),
            popularAdapter,
            viewModel.popularMoviesStream,
            R.drawable.ic_popular
        )
        binding.sectionUpcoming.bind(
            "Upcoming",
            UpcomingMoviePagingStrategy(),
            upcomingAdapter,
            viewModel.upcomingMoviesStream,
            R.drawable.ic_upcoming
        )
        binding.sectionTopRated.bind(
            "Top Rated",
            TopRatedMoviePagingStrategy(),
            topRatedAdapter,
            viewModel.topRatedMovieStateStream,
            R.drawable.ic_top_rated
        )
    }


    private fun SectionSliderHorizontalBinding.bind(
        title: String,
        pagingStrategy: MoviePagingStrategy,
        adapter: MovieAdapter,
        movieState: LiveData<MovieState>,
        iconResId: Int
    ) {
        sectionAction.setOnClickListener {
            navigateToMovieList(pagingStrategy, "$title Movies")
        }

        list.layoutManager = LinearLayoutManager(context, LinearLayoutManager.HORIZONTAL, false)
        list.adapter = adapter
        sectionTitle.text = title

        imageView.setBackgroundResource(iconResId)

        movieState.observe(viewLifecycleOwner) { state ->
            if (state != null) {
                binding.loader.isVisible = state.isLoading

                if (state.showError) {
                    handleError()
                } else {
                    adapter.submitList(state.movies)
                }

            }
        }
    }

    private fun handleError() {
        viewModel.hideErrors()
        Toast.makeText(context, "Movie loading error. Try to restart app.", Toast.LENGTH_LONG)
            .show()
    }

    private fun navigateToMovieList(strategy: MoviePagingStrategy, sectionTitle: String) {
        val directions =
            MainFragmentDirections.actionToMovieList(strategy, sectionTitle)
        findNavController().navigate(directions)
    }


    private fun navigateToDetail(movie: Movie) {
        val directions =
            MainFragmentDirections.actionToDetail(
                movie.id.toLong(),
                movie.title
            )
        findNavController().navigate(directions)
    }


    private fun navigateToSearch() {
        val directions =
            MainFragmentDirections.actionToSearch()
        findNavController().navigate(directions)
    }

    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        super.onCreateOptionsMenu(menu, inflater)
        inflater.inflate(R.menu.menu_base, menu)

    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        return when (item.itemId) {
            R.id.list_search -> {
                // navigate to search screen
                navigateToSearch()
                true
            }
            else -> {
                super.onOptionsItemSelected(item)
            }
        }
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }
}
