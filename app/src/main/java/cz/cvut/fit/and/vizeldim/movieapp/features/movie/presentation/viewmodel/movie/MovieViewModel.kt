package cz.cvut.fit.and.vizeldim.movieapp.features.movie.presentation.viewmodel.movie

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import cz.cvut.fit.and.vizeldim.movieapp.features.movie.data.IMovieRepository
import cz.cvut.fit.and.vizeldim.movieapp.features.movie.domain.Movie
import kotlinx.coroutines.launch

class MovieViewModel(
    private val movieRepository: IMovieRepository
) : ViewModel() {
    private val _popularMovieStateStream: MutableLiveData<MovieState> = MutableLiveData(MovieState())
    val popularMoviesStream: LiveData<MovieState> = _popularMovieStateStream

    private val _upcomingMovieStateStream: MutableLiveData<MovieState> = MutableLiveData(MovieState())
    val upcomingMoviesStream: LiveData<MovieState> = _upcomingMovieStateStream

    private val _topRatedMovieStateStream: MutableLiveData<MovieState> = MutableLiveData(MovieState())
    val topRatedMovieStateStream: LiveData<MovieState> = _topRatedMovieStateStream

    init {
        listOf( _popularMovieStateStream to movieRepository::getPopularPage,
                _upcomingMovieStateStream to movieRepository::getUpcomingPage,
                _topRatedMovieStateStream to movieRepository::getTopRatedPage)
            .forEach{(movieStateStream, movieGetter) -> loadMovies(movieStateStream, movieGetter)}
    }

    private fun loadMovies(movieStateStream: MutableLiveData<MovieState>,
                           movieGetter: suspend (page: Int)->List<Movie>) {
        viewModelScope.launch {
            movieStateStream.value = movieStateStream.value?.copy(isLoading = true)

            try {
                movieStateStream.value = movieStateStream.value?.copy(movies = movieGetter(1))
            } catch (t: Throwable) {
                movieStateStream.value = movieStateStream.value?.copy(showError = true)
            } finally {
                movieStateStream.value = movieStateStream.value?.copy(isLoading = false)
            }
        }
    }

    fun hideErrors() {
        _topRatedMovieStateStream.value = _topRatedMovieStateStream.value?.copy(showError = false)
        _popularMovieStateStream.value = _topRatedMovieStateStream.value?.copy(showError = false)
        _upcomingMovieStateStream.value = _topRatedMovieStateStream.value?.copy(showError = false)
    }

}

data class MovieState(
    val movies: List<Movie> = emptyList(),
    val isLoading: Boolean = false,
    val showError: Boolean = false,
)


