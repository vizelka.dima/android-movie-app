package cz.cvut.fit.and.vizeldim.movieapp.features.movie.data.room

import androidx.room.*
import kotlinx.coroutines.flow.Flow

@Dao
abstract class MovieCollectionDao {
    // --- CREATE ---
    @Insert
    abstract suspend fun create(collection: DbMovieCollection): Long

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    abstract suspend fun insertMovie(movie: DbMovie)

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    abstract suspend fun insertMovies(movies: List<DbMovie>): List<Long>

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    abstract suspend fun insertMovieToCollection(ref: CollectionMovieCrossRef): Long

    @Insert
    abstract suspend fun insertMoviesToCollection(refs: List<CollectionMovieCrossRef>)


    // --- READ ---
    // stream
    @Transaction
    @Query("SELECT * FROM movieCollections where collectionId=:id")
    abstract fun streamById(id: Long): Flow<DbMovieCollectionWithMovies>

    @Query("SELECT * FROM movieCollections")
    abstract fun streamAll(): Flow<List<DbMovieCollection>>

    @Query("SELECT * FROM movieCollections WHERE collectionId = :collectionId")
    abstract fun streamCollectionById(collectionId: Long): Flow<DbMovieCollection>


    // --- UPDATE ---
    @Query("UPDATE movieCollections SET name = :name WHERE collectionId = :collectionId")
    abstract suspend fun updateCollectionName(collectionId: Long, name: String): Int


    // --- DELETE ---
    @Query("DELETE FROM movieCollections where collectionId=:id ")
    abstract suspend fun deleteById(id: Long)

    @Query("DELETE FROM collectionMovieCrossRef WHERE collectionId = :collectionId AND movieId = :movieId")
    abstract suspend fun deleteMovieFromCollection(collectionId: Long, movieId: Long): Int

    @Query("DELETE FROM collectionMovieCrossRef WHERE collectionId = :collectionId")
    abstract suspend fun deleteAllFromCollection(collectionId: Long)
}