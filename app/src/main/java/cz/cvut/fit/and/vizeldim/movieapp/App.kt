package cz.cvut.fit.and.vizeldim.movieapp

import android.app.Application
import cz.cvut.fit.and.vizeldim.movieapp.features.movie.di.movieModule
import cz.cvut.fit.and.vizeldim.movieapp.shared.di.sharedModule
import org.koin.android.ext.koin.androidContext
import org.koin.core.context.startKoin

class App : Application() {

    override fun onCreate() {
        super.onCreate()

        startKoin {
            androidContext(androidContext = this@App)
            modules(listOf(sharedModule, movieModule))
        }
    }
}