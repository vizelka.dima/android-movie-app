package cz.cvut.fit.and.vizeldim.movieapp.features.movie.presentation.adapter.movie

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.paging.PagingDataAdapter
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import cz.cvut.fit.and.vizeldim.movieapp.databinding.ItemSearchMovieBinding
import cz.cvut.fit.and.vizeldim.movieapp.features.movie.domain.Movie


class MoviePagingAdapter(
    private val onMovieClick: (Movie) -> Unit
) : PagingDataAdapter<Movie, MoviePagingAdapter.MovieHolder>(MovieAdapter.MovieDiffCallback()) {

    init {
        // Correctly restores scroll position on screen rotation.
        stateRestorationPolicy = StateRestorationPolicy.PREVENT_WHEN_EMPTY
    }

    override fun onCreateViewHolder(viewGroup: ViewGroup, viewType: Int): MovieHolder {
        val binding = ItemSearchMovieBinding.inflate(
            LayoutInflater.from(viewGroup.context),
            viewGroup,
            false
        )
        return MovieHolder(binding, onMovieClick)
    }

    override fun onBindViewHolder(movieHolder: MovieHolder, position: Int) {
        val movie = getItem(position)
        movieHolder.bind(movie)
    }

    class MovieHolder(
        private val binding: ItemSearchMovieBinding,
        private val onMovieClick: (Movie) -> Unit
    ) :
        RecyclerView.ViewHolder(binding.root) {

        fun bind(movie: Movie?) {
            movie?.posterImg?.let {
                Glide.with(itemView).load("https://image.tmdb.org/t/p/w200${it}").into(binding.img)
            }
            movie?.title?.let { binding.txtName.text = it }

            movie?.let {
                binding.root.setOnClickListener { onMovieClick(movie) }
            }
        }
    }
}
