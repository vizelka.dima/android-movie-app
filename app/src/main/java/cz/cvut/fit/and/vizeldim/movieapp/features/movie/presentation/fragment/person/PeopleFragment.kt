package cz.cvut.fit.and.vizeldim.movieapp.features.movie.presentation.fragment.person

import android.os.Bundle
import android.view.*
import androidx.fragment.app.Fragment
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import cz.cvut.fit.and.vizeldim.movieapp.R
import cz.cvut.fit.and.vizeldim.movieapp.databinding.FragmentPeopleBinding
import cz.cvut.fit.and.vizeldim.movieapp.features.movie.domain.Person
import cz.cvut.fit.and.vizeldim.movieapp.features.movie.presentation.adapter.person.PersonPagingAdapter
import cz.cvut.fit.and.vizeldim.movieapp.features.movie.presentation.viewmodel.person.PeopleViewModel
import org.koin.androidx.viewmodel.ext.android.viewModel

class PeopleFragment : Fragment() {
    private var _binding: FragmentPeopleBinding? = null

    // This property is only valid between onCreateView and
    // onDestroyView.
    private val binding get() = _binding!!

    private val pagingAdapter = PersonPagingAdapter(::navigateToDetail)

    private val viewModel by viewModel<PeopleViewModel>()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setHasOptionsMenu(true)
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        _binding = FragmentPeopleBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)


        binding.list.layoutManager = LinearLayoutManager(context)
        binding.list.adapter = pagingAdapter

        viewModel.load(pagingAdapter)
    }

    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        super.onCreateOptionsMenu(menu, inflater)
        inflater.inflate(R.menu.menu_base, menu)

    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        return when (item.itemId) {
            R.id.list_search -> {
                // navigate to search screen
                navigateToSearch()
                true
            }
            else -> {
                super.onOptionsItemSelected(item)
            }
        }
    }

    private fun navigateToDetail(person: Person) {
        val directions =
            PeopleFragmentDirections.actionToPersonMovies(
                person.id,
                person.name
            )
        findNavController().navigate(directions)
    }

    private fun navigateToSearch() {
        val directions =
            PeopleFragmentDirections.actionToSearch()
        findNavController().navigate(directions)
    }
}