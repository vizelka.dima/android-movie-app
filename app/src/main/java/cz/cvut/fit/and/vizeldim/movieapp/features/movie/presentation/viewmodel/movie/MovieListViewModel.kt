package cz.cvut.fit.and.vizeldim.movieapp.features.movie.presentation.viewmodel.movie

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import androidx.paging.Pager
import androidx.paging.PagingConfig
import androidx.paging.PagingData
import androidx.paging.cachedIn
import cz.cvut.fit.and.vizeldim.movieapp.features.movie.data.IMovieRepository
import cz.cvut.fit.and.vizeldim.movieapp.features.movie.data.paging.movie.MoviePagingSource
import cz.cvut.fit.and.vizeldim.movieapp.features.movie.data.paging.movie.MoviePagingStrategy
import cz.cvut.fit.and.vizeldim.movieapp.features.movie.domain.Movie
import cz.cvut.fit.and.vizeldim.movieapp.features.movie.presentation.adapter.movie.MoviePagingAdapter
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.collectLatest
import kotlinx.coroutines.launch

class MovieListViewModel(private val repository: IMovieRepository): ViewModel() {
    private var _pagingData : Flow<PagingData<Movie>>? = null

    fun load(adapter: MoviePagingAdapter, pagingStrategy: MoviePagingStrategy) {
        if (_pagingData == null) {
            _pagingData = Pager(
                PagingConfig(pageSize = 5) ,
                pagingSourceFactory = { MoviePagingSource(repository, pagingStrategy) })
                .flow.cachedIn(viewModelScope)
        }

        viewModelScope.launch {
            _pagingData!!.collectLatest {
                adapter.submitData(it)
            }
        }

    }
}
