package cz.cvut.fit.and.vizeldim.movieapp.features.movie.domain

data class Movie(
    val id: Int,
    val title: String,
    val detailImg: String?,
    val posterImg: String?,
    val avgVotes: Double,
    val runtime: Int?,
    val releaseDate: String?,
    val genres: List<Genre>,
    val video: Video?,
    val casts: List<Person>
) {
    val runtimeStr: String
        get() {
            if (runtime == null) {
                return ""
            }
            val hours = runtime / 60
            val mins = runtime % 60

            val hString = if (hours > 0) "${hours}h" else ""

            return "$hString ${mins}m"
        }
}
