package cz.cvut.fit.and.vizeldim.movieapp.features.movie.sharecode

import cz.cvut.fit.and.vizeldim.movieapp.features.movie.domain.SimpleMovie

interface CodeContentParserInterface {
    fun parse(codeContentJsonStr: String): CodeContent?

    fun stringify(codeContent: CodeContent): String
}

data class CodeContent(
    val collectionName: String,
    val movies: List<SimpleMovie>
)

class CodeContentParseException : Throwable()