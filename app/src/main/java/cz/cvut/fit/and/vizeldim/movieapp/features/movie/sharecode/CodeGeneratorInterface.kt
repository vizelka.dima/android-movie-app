package cz.cvut.fit.and.vizeldim.movieapp.features.movie.sharecode

class CodeGeneratorException : Throwable()

interface CodeGeneratorInterface {
    suspend fun generate(codeContentStr: String): CodeInterface
}