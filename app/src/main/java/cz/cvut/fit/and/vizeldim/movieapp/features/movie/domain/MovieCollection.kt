package cz.cvut.fit.and.vizeldim.movieapp.features.movie.domain

data class MovieCollection(
    val id: Long,
    val name: String
)

