package cz.cvut.fit.and.vizeldim.movieapp.shared.di

import androidx.room.Room
import cz.cvut.fit.and.vizeldim.movieapp.shared.data.room.MovieCollectionDatabase
import okhttp3.Interceptor
import okhttp3.OkHttpClient
import org.koin.android.ext.koin.androidContext
import org.koin.dsl.module
import retrofit2.Retrofit
import retrofit2.converter.moshi.MoshiConverterFactory


val sharedModule = module {
    single<Retrofit> {
        val apiKeyInterceptor = Interceptor {
            var req = it.request()
            val url = req.url.newBuilder()
                .addQueryParameter("api_key", "c15c0d55d59b1958598a91e8da15449e").build()
            req = req.newBuilder().url(url).build()

            it.proceed(req)
        }

        Retrofit.Builder()
            .baseUrl("https://api.themoviedb.org/3/")
            .client(
                OkHttpClient.Builder()
                    .addInterceptor(apiKeyInterceptor)
                    .build()
            )
            .addConverterFactory(MoshiConverterFactory.create().withNullSerialization())
            .build()
    }

    single {
        Room.databaseBuilder(
            androidContext(),
            MovieCollectionDatabase::class.java,
            "movieCollections"
        ).build()
    }
}