package cz.cvut.fit.and.vizeldim.movieapp.features.movie.presentation.viewmodel.person

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import androidx.paging.Pager
import androidx.paging.PagingConfig
import androidx.paging.PagingData
import androidx.paging.cachedIn
import cz.cvut.fit.and.vizeldim.movieapp.features.movie.data.paging.person.PersonPagingSource
import cz.cvut.fit.and.vizeldim.movieapp.features.movie.domain.Person
import cz.cvut.fit.and.vizeldim.movieapp.features.movie.presentation.adapter.person.PersonPagingAdapter
import kotlinx.coroutines.flow.*
import kotlinx.coroutines.launch

class PeopleViewModel(
    private val personPagingSource: PersonPagingSource
) : ViewModel() {
    private val _pagingData : Flow<PagingData<Person>> = Pager(
        PagingConfig(pageSize = 5) ,
        pagingSourceFactory = {personPagingSource}).flow.cachedIn(viewModelScope)

    fun load(pagingAdapter: PersonPagingAdapter) {
        viewModelScope.launch {
            _pagingData.collectLatest {
                pagingAdapter.submitData(it)
            }
        }

    }
}
