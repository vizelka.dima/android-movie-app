package cz.cvut.fit.and.vizeldim.movieapp.features.movie.data.retrofit

import cz.cvut.fit.and.vizeldim.movieapp.features.movie.data.MovieRemoteDataSource
import cz.cvut.fit.and.vizeldim.movieapp.features.movie.domain.Movie
import cz.cvut.fit.and.vizeldim.movieapp.features.movie.domain.Person

class MovieRetrofitDataSource(
    private val movieApiDescription: MovieApiDescription
) : MovieRemoteDataSource {
    override suspend fun getPopularPage(page: Int): List<Movie> {
        return movieApiDescription
            .getPopularPage(page)
            .toDomain()
    }

    override suspend fun getTopRatedPage(page: Int): List<Movie> {
        return movieApiDescription
            .getTopRatedPage(page)
            .toDomain()
    }

    override suspend fun getUpcomingPage(page: Int): List<Movie> {
        return movieApiDescription
            .getUpcomingPage(page)
            .toDomain()
    }

    override suspend fun getPageByName(
        name: String,
        page: Int
    ): List<Movie> {
        return movieApiDescription
            .getPageByName(name, page)
            .toDomain()
    }

    override suspend fun getById(id: Long): Movie {
        return movieApiDescription
            .getById(id)
            .toDomain()
    }

    override suspend fun getPopularPersonPage(page: Int): List<Person> {
        return movieApiDescription
            .getPopularPersonPage(page)
            .toDomain()
    }

    override suspend fun getPersonPageByName(
        name: String,
        page: Int
    ): List<Person> {
        return movieApiDescription
            .getPersonPageByName(name, page)
            .toDomain()
    }

    override suspend fun getPageByPersonId(
        id: Long,
        page: Int
    ): List<Movie> {
        return movieApiDescription
            .getPageByPersonId(id, page)
            .toDomain()
    }
}


