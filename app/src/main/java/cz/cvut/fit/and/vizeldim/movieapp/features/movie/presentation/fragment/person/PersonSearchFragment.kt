package cz.cvut.fit.and.vizeldim.movieapp.features.movie.presentation.fragment.person

import android.os.Bundle
import android.view.*
import androidx.appcompat.widget.SearchView
import androidx.core.view.isVisible
import androidx.fragment.app.Fragment
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import com.google.android.material.snackbar.Snackbar
import cz.cvut.fit.and.vizeldim.movieapp.R
import cz.cvut.fit.and.vizeldim.movieapp.databinding.FragmentSearchBinding
import cz.cvut.fit.and.vizeldim.movieapp.features.movie.domain.Person
import cz.cvut.fit.and.vizeldim.movieapp.features.movie.presentation.adapter.person.SearchPersonAdapter
import cz.cvut.fit.and.vizeldim.movieapp.features.movie.presentation.viewmodel.person.SearchPeopleViewModel
import org.koin.androidx.viewmodel.ext.android.viewModel

class PersonSearchFragment : Fragment() {
    private var _binding: FragmentSearchBinding? = null

    // This property is only valid between onCreateView and
    // onDestroyView.
    private val binding get() = _binding!!

    private val adapter = SearchPersonAdapter(::navigateToDetail)

    private val viewModel by viewModel<SearchPeopleViewModel>()

    private lateinit var searchView: SearchView


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setHasOptionsMenu(true)
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        _binding = FragmentSearchBinding.inflate(inflater, container, false)
        return binding.root

    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        binding.searchCharacters.layoutManager = LinearLayoutManager(context)
        binding.searchCharacters.adapter = adapter

        viewModel.searchStateStream.observe(viewLifecycleOwner) { state ->
            if (state != null) {
                binding.searchProgressBar.isVisible = state.isLoading

                if (state.showError) {
                    Snackbar.make(binding.root, "Not Found", Snackbar.LENGTH_SHORT).show()
                    viewModel.hideError()
                } else {
                    adapter.submitList(state.foundPeople)
                }
            }
        }
    }

    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        super.onCreateOptionsMenu(menu, inflater)
        inflater.inflate(R.menu.menu_search, menu)

        searchView = menu.findItem(R.id.search)?.actionView as SearchView


        searchView.isIconified = false
        searchView.requestFocusFromTouch()

        searchView.setOnCloseListener {
            viewModel.clearFoundResults()

            searchView.setQuery("", false)
            true
        }

        searchView.setOnQueryTextListener(QueryChangeListener(searchView, viewModel))
    }


    class QueryChangeListener(
        private val searchView: SearchView,
        private val viewModel: SearchPeopleViewModel
    ) : SearchView.OnQueryTextListener {

        override fun onQueryTextSubmit(query: String?): Boolean {
            query?.let { viewModel.searchByName(it) }
            searchView.clearFocus()
            return true
        }

        override fun onQueryTextChange(query: String?): Boolean {
            query?.let { viewModel.searchByName(it) }
            return true
        }

    }

    private fun navigateToDetail(person: Person) {
        val directions =
            PersonSearchFragmentDirections.actionToDetail(
                person.id,
                person.name
            )
        findNavController().navigate(directions)
    }


    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
        searchView.clearFocus()

    }

}