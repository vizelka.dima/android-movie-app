package cz.cvut.fit.and.vizeldim.movieapp.features.movie.domain

data class Person(
    val id: Long,
    val name: String,
    val img: String?,
    val knownForDepartment: String?
)
