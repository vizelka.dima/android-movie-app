package cz.cvut.fit.and.vizeldim.movieapp.features.movie.presentation.component

import cz.cvut.fit.and.vizeldim.movieapp.R
import cz.cvut.fit.and.vizeldim.movieapp.databinding.TemplateRatingStarsBinding

class RatingComponent(
    private val binding: TemplateRatingStarsBinding,
    private val ratingResourceActive: Int = R.drawable.ic_star_filled,
    private val ratingResourceDisabled: Int = R.drawable.ic_star_filled_inactive
    ) {

    fun render(ratingVal: Double) {
        binding.apply {
            listOf(
                1 to starOne, 2 to starTwo, 4 to starThree, 6 to starFour, 8 to starFive
            ).forEach {
                (max, img) ->
                    img.setBackgroundResource(getStar(max, ratingVal))
            }
        }
    }
    
    private fun getStar(max: Int, cur: Double) : Int {
        return if(cur > max) ratingResourceActive
            else ratingResourceDisabled
    }
}