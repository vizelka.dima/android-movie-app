package cz.cvut.fit.and.vizeldim.movieapp

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.core.view.isVisible
import androidx.navigation.findNavController
import androidx.navigation.fragment.NavHostFragment
import androidx.navigation.fragment.findNavController
import androidx.navigation.ui.AppBarConfiguration
import androidx.navigation.ui.NavigationUI
import androidx.navigation.ui.NavigationUiSaveStateControl
import androidx.navigation.ui.navigateUp
import com.google.firebase.analytics.FirebaseAnalytics
import cz.cvut.fit.and.vizeldim.movieapp.databinding.ActivityMainBinding


class MainActivity : AppCompatActivity() {
    private lateinit var mFirebaseAnalytics: FirebaseAnalytics

    private lateinit var appBarConfiguration: AppBarConfiguration
    private lateinit var binding: ActivityMainBinding


    private val topLevelDestinationIds = setOf(
        R.id.fragment_movies,
        R.id.fragment_people,
        R.id.fragment_collections
    )

    private val destinationIdsWithoutBottomNavigation = setOf(
        R.id.fragment_movie_search,
        R.id.fragment_movie_detail,
        R.id.fragment_collection_edit
    )

    @OptIn(NavigationUiSaveStateControl::class)
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        mFirebaseAnalytics = FirebaseAnalytics.getInstance(this)


        binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)


        val navHostFragment =
            supportFragmentManager.findFragmentById(R.id.nav_host_fragment_content_main) as NavHostFragment
        val navController = navHostFragment.findNavController()

        appBarConfiguration = AppBarConfiguration(topLevelDestinationIds = topLevelDestinationIds)
        setSupportActionBar(binding.toolbar)

        NavigationUI.setupActionBarWithNavController(this, navController, appBarConfiguration)
        NavigationUI.setupWithNavController(
            binding.bottomNavigation,
            navController,
            saveState = false
        )


        navController.addOnDestinationChangedListener { _, destination, _ ->
            val isDestinationWithoutBottomNavigation =
                destination.id in destinationIdsWithoutBottomNavigation


            binding.bottomNavigation.isVisible = !isDestinationWithoutBottomNavigation
        }


    }

    override fun onSupportNavigateUp(): Boolean {
        val navController = findNavController(R.id.nav_host_fragment_content_main)

        if (navController.currentDestination?.id == R.id.fragment_collection_detail) {
            navController.navigate(R.id.fragment_collections)
            return true
        }

        return navController.navigateUp(appBarConfiguration)
                || super.onSupportNavigateUp()
    }
}