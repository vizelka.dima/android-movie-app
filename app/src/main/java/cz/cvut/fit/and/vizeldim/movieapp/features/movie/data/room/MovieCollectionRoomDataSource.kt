package cz.cvut.fit.and.vizeldim.movieapp.features.movie.data.room

import cz.cvut.fit.and.vizeldim.movieapp.features.movie.data.MovieCollectionDbDataSource
import cz.cvut.fit.and.vizeldim.movieapp.features.movie.domain.MovieCollection
import cz.cvut.fit.and.vizeldim.movieapp.features.movie.domain.SimpleMovie
import kotlinx.coroutines.flow.Flow

class MovieCollectionRoomDataSource(
    val dao: MovieCollectionDao
) : MovieCollectionDbDataSource {
    override fun streamAll(): Flow<List<MovieCollection>> =
        dao.streamAll()
            .toDomain()

    override suspend fun create(name: String): Long {
        return dao.create(DbMovieCollection(name))
    }

    override suspend fun create(name: String, movies: List<SimpleMovie>): Long {
        val collectionId: Long = dao.create(DbMovieCollection(name))

        val movieIds: List<Long> = dao.insertMovies(movies.map {
            DbMovie(it.id, it.title, it.img)
        })

        dao.insertMoviesToCollection(
            movieIds.map {
                CollectionMovieCrossRef(
                    movieId = it,
                    collectionId = collectionId
                )
            }
        )

        return collectionId
    }

    override suspend fun remove(collectionId: Long) {
        dao.deleteAllFromCollection(collectionId)
        dao.deleteById(collectionId)
    }

    override suspend fun addMovie(collectionId: Long, movie: SimpleMovie): Long {
        dao.insertMovie(DbMovie(movie.id, movie.title, movie.img))
        return dao.insertMovieToCollection(CollectionMovieCrossRef(movie.id, collectionId))
    }

    override suspend fun removeMovie(collectionId: Long, movie: SimpleMovie): Boolean {
        return dao.deleteMovieFromCollection(collectionId, movie.id) == 1
    }

    override suspend fun updateCollectionName(collectionId: Long, name: String): Boolean {
        return dao.updateCollectionName(collectionId, name) == 1
    }

    override fun streamMovies(collectionId: Long): Flow<List<SimpleMovie>> =
        dao.streamById(collectionId).toDomain()

    override fun streamCollectionById(collectionId: Long): Flow<MovieCollection> {
        return dao.streamCollectionById(collectionId).toDomain()
    }
}