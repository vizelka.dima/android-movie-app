package cz.cvut.fit.and.vizeldim.movieapp.features.movie.presentation.fragment.collection

import android.Manifest
import android.content.pm.PackageManager
import android.os.Bundle
import android.view.*
import android.widget.Toast
import androidx.activity.result.contract.ActivityResultContracts
import androidx.appcompat.app.AlertDialog
import androidx.core.content.ContextCompat
import androidx.fragment.app.Fragment
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.GridLayoutManager
import com.google.android.material.dialog.MaterialAlertDialogBuilder
import com.google.android.material.snackbar.Snackbar
import cz.cvut.fit.and.vizeldim.movieapp.R
import cz.cvut.fit.and.vizeldim.movieapp.databinding.DialogAddCollectionBinding
import cz.cvut.fit.and.vizeldim.movieapp.databinding.FragmentCollectionListBinding
import cz.cvut.fit.and.vizeldim.movieapp.features.movie.domain.MovieCollection
import cz.cvut.fit.and.vizeldim.movieapp.features.movie.presentation.adapter.collection.CollectionAdapter
import cz.cvut.fit.and.vizeldim.movieapp.features.movie.presentation.viewmodel.collection.CollectionsViewModel
import org.koin.androidx.viewmodel.ext.android.viewModel


class CollectionFragment : Fragment() {
    private var _binding: FragmentCollectionListBinding? = null

    // This property is only valid between onCreateView and
    // onDestroyView.
    private val binding get() = _binding!!

    private val viewModel by viewModel<CollectionsViewModel>()

    private val adapter = CollectionAdapter(::navigateToDetail)

    private val permissionsLauncher =
        registerForActivityResult(ActivityResultContracts.RequestMultiplePermissions()) { permissions ->
            if (permissions[Manifest.permission.CAMERA] == true) {
                navigateToImport()
            } else {
                handleCameraPermissionDenied()
            }
        }

    private fun handleCameraPermissionDenied() {
        Toast.makeText(context, "Camera is required to scan QR code", Toast.LENGTH_LONG).show()
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setHasOptionsMenu(true)
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        _binding = FragmentCollectionListBinding.inflate(inflater, container, false)

        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        configureAdapter()
        loadCollections()
    }

    private fun configureAdapter() {
        binding.list.layoutManager = GridLayoutManager(context, 2)
        binding.list.adapter = adapter
    }

    private fun loadCollections() {
        viewModel.collectionsStateStream.observe(viewLifecycleOwner) { state ->
            state?.let {
                adapter.submitList(it.collections)
            }
        }
    }

    private fun navigateToDetail(movieCollection: MovieCollection) {
        val directions = CollectionFragmentDirections.actionToCollectionDetail(
            movieCollection.id,
            movieCollection.name
        )
        findNavController().navigate(directions)
    }

    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        super.onCreateOptionsMenu(menu, inflater)
        inflater.inflate(R.menu.menu_collections, menu)

        if (!(requireActivity().applicationContext.packageManager.hasSystemFeature(PackageManager.FEATURE_CAMERA_ANY))) {
            menu.getItem(R.id.menu_item_import_collection).isVisible = false
        }
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        return when (item.itemId) {
            R.id.menu_item_add_movie -> {
                // open dialog with add list form
                showAddDialog()
                true
            }
            R.id.menu_item_import_collection -> {
                navigateToImportWithPermission()
                true
            }
            else -> {
                super.onOptionsItemSelected(item)
            }
        }
    }

    private fun navigateToImportWithPermission() {
        permissionsLauncher.launch(arrayOf(Manifest.permission.CAMERA))
    }

    private val collectionAddDialog: AlertDialog by lazy {
        val dialogBinding =
            DialogAddCollectionBinding.inflate(LayoutInflater.from(requireContext()))
        val dialogBuilder = MaterialAlertDialogBuilder(requireContext())
        val nameInput = dialogBinding.plainTextInput

        dialogBuilder.setView(dialogBinding.root)
            .setTitle("Add Movie Collection")
            .setMessage("Enter collection name")
            .setPositiveButton("Add") { dialog, _ ->
                val nameStr = nameInput.text.toString()

                if (nameStr.isEmpty()) {
                    Snackbar.make(requireView(), "Invalid Name.", 1000).show()
                } else {
                    viewModel.addCollection(nameStr)
                    nameInput.setText("")

                    dialog.dismiss()
                }
            }
            .setNegativeButton("Cancel") { dialog, _ ->
                nameInput.setText("")
                dialog.dismiss()
            }
            .create()
    }

    private fun showAddDialog() {
        collectionAddDialog.show()
    }

    private fun navigateToImport() {
        if (isCameraPermissionGranted) {
            val directions = CollectionFragmentDirections.actionToCollectionImport()
            findNavController().navigate(directions)
        } else {
            handleCameraPermissionDenied()
        }
    }

    private val isCameraPermissionGranted: Boolean
        get() {
            return ContextCompat.checkSelfPermission(
                requireContext(),
                Manifest.permission.CAMERA
            ) ==
                    PackageManager.PERMISSION_GRANTED
        }
}