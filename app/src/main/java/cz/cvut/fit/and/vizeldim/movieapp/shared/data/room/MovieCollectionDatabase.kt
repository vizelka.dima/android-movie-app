package cz.cvut.fit.and.vizeldim.movieapp.shared.data.room

import androidx.room.Database
import androidx.room.RoomDatabase
import cz.cvut.fit.and.vizeldim.movieapp.features.movie.data.room.CollectionMovieCrossRef
import cz.cvut.fit.and.vizeldim.movieapp.features.movie.data.room.DbMovie
import cz.cvut.fit.and.vizeldim.movieapp.features.movie.data.room.DbMovieCollection
import cz.cvut.fit.and.vizeldim.movieapp.features.movie.data.room.MovieCollectionDao

@Database(
    entities = [DbMovie::class,
        DbMovieCollection::class,
        CollectionMovieCrossRef::class],
    version = 1,
    exportSchema = false
)
abstract class MovieCollectionDatabase : RoomDatabase() {
    abstract fun movieCollectionDao(): MovieCollectionDao
}