package cz.cvut.fit.and.vizeldim.movieapp.features.movie.presentation.viewmodel.person

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import cz.cvut.fit.and.vizeldim.movieapp.features.movie.data.IMovieRepository
import cz.cvut.fit.and.vizeldim.movieapp.features.movie.domain.Person
import kotlinx.coroutines.launch

class SearchPeopleViewModel(
    private val movieRepository: IMovieRepository
) : ViewModel() {
    private val _searchStateStream: MutableLiveData<SearchPeopleState> = MutableLiveData(
        SearchPeopleState()
    )
    val searchStateStream: LiveData<SearchPeopleState> = _searchStateStream


    fun searchByName(name: String) {
        if (_searchStateStream.value?.searchName != name) {
            if(name.isBlank()) {
                _searchStateStream.value =
                    _searchStateStream.value?.copy(searchName = name, foundPeople = emptyList())
            } else {
                viewModelScope.launch {
                    _searchStateStream.value =
                        _searchStateStream.value?.copy(isLoading = true, searchName = name)

                    try {
                        _searchStateStream.value = _searchStateStream.value?.copy(
                            foundPeople = movieRepository.getPersonPageByName(name),
                            searchName = name
                        )
                    } catch (t: Throwable) {
                        _searchStateStream.value = _searchStateStream.value?.copy(
                            foundPeople = emptyList(),
                            showError = true
                        )
                    } finally {
                        _searchStateStream.value = _searchStateStream.value?.copy(isLoading = false)
                    }
                }
            }
        }
    }

    fun hideError() {
        _searchStateStream.value = _searchStateStream.value?.copy(showError = false)
    }

    fun clearFoundResults() {
        _searchStateStream.value =
            _searchStateStream.value?.copy(foundPeople = emptyList(), searchName = "")
    }

}


data class SearchPeopleState(
    val foundPeople: List<Person> = emptyList(),
    val searchName: String = "",
    val isLoading: Boolean = false,
    val showError: Boolean = false
)
