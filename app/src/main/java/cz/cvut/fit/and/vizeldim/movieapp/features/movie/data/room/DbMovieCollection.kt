package cz.cvut.fit.and.vizeldim.movieapp.features.movie.data.room

import androidx.room.*

@Entity(tableName = "movieCollections")
data class DbMovieCollection(
    val name: String,
) {
    @PrimaryKey(autoGenerate = true)
    var collectionId: Long = 0
}

@Entity(
    primaryKeys = ["collectionId", "movieId"],
    indices = [
        Index(value = arrayOf("movieId"))
    ]
)
data class CollectionMovieCrossRef(
    val movieId: Long,
    val collectionId: Long
)

data class DbMovieCollectionWithMovies(
    @Embedded val collection: DbMovieCollection,
    @Relation(
        parentColumn = "collectionId",
        entityColumn = "movieId",
        associateBy = Junction(CollectionMovieCrossRef::class)
    )
    val movies: List<DbMovie>
)