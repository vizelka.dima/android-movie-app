package cz.cvut.fit.and.vizeldim.movieapp.features.movie.data.retrofit

import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass

@JsonClass(generateAdapter = true)
data class ApiMovie(
    val id: Int,
    val title: String,
    @Json(name = "backdrop_path") val detailImg: String?,
    @Json(name = "poster_path") val posterImg: String?,
    @Json(name = "vote_average") val avgVotes: Double,
    val runtime: Int?,
    @Json(name = "release_date") val releaseDate: String?,
    val genres: List<ApiGenre>?,
    @Json(name = "videos") val videos: ApiVideoList?,
    val credits: ApiCreditList?
)

@JsonClass(generateAdapter = true)
class ApiMovieListResponse(
    val results: List<ApiMovie>
)

@JsonClass(generateAdapter = true)
data class ApiCreditList(
    val cast: List<ApiPerson>
)

@JsonClass(generateAdapter = true)
data class ApiGenre(
    val id: Int,
    val name: String
)

@JsonClass(generateAdapter = true)
data class ApiVideoList(
    val results: List<ApiVideo>
)


@JsonClass(generateAdapter = true)
data class ApiVideo(
    val site: String,
    val key: String
)


