package cz.cvut.fit.and.vizeldim.movieapp.features.movie.presentation.adapter.movie

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.ListAdapter
import cz.cvut.fit.and.vizeldim.movieapp.databinding.ItemSearchMovieBinding
import cz.cvut.fit.and.vizeldim.movieapp.features.movie.domain.Movie


class SearchMovieAdapter(
    private val onMovieClick: (Movie) -> Unit
) : ListAdapter<Movie, MoviePagingAdapter.MovieHolder>(MovieAdapter.MovieDiffCallback()) {

    init {
        // Correctly restores scroll position on screen rotation.
        stateRestorationPolicy = StateRestorationPolicy.PREVENT_WHEN_EMPTY
    }

    override fun onCreateViewHolder(
        viewGroup: ViewGroup,
        viewType: Int
    ): MoviePagingAdapter.MovieHolder {
        val binding = ItemSearchMovieBinding.inflate(
            LayoutInflater.from(viewGroup.context),
            viewGroup,
            false
        )
        return MoviePagingAdapter.MovieHolder(binding, onMovieClick)
    }


    override fun onBindViewHolder(movieHolder: MoviePagingAdapter.MovieHolder, position: Int) {
        val movie = getItem(position)
        movieHolder.bind(movie)
    }
}
