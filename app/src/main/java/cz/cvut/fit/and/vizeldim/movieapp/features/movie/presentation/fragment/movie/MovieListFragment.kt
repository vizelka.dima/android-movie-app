package cz.cvut.fit.and.vizeldim.movieapp.features.movie.presentation.fragment.movie

import android.os.Bundle
import android.view.*
import androidx.fragment.app.Fragment
import androidx.navigation.fragment.findNavController
import androidx.navigation.fragment.navArgs
import androidx.recyclerview.widget.LinearLayoutManager
import cz.cvut.fit.and.vizeldim.movieapp.R
import cz.cvut.fit.and.vizeldim.movieapp.databinding.FragmentMovieListBinding
import cz.cvut.fit.and.vizeldim.movieapp.features.movie.domain.Movie
import cz.cvut.fit.and.vizeldim.movieapp.features.movie.presentation.adapter.movie.MoviePagingAdapter
import cz.cvut.fit.and.vizeldim.movieapp.features.movie.presentation.viewmodel.movie.MovieListViewModel
import org.koin.androidx.viewmodel.ext.android.viewModel

class MovieListFragment : Fragment() {
    private val args: MovieListFragmentArgs by navArgs()

    private var _binding: FragmentMovieListBinding? = null

    // This property is only valid between onCreateView and
    // onDestroyView.
    private val binding get() = _binding!!

    private val adapter = MoviePagingAdapter(::navigateToDetail)

    private val viewModel by viewModel<MovieListViewModel>()


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setHasOptionsMenu(true)
    }


    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        _binding = FragmentMovieListBinding.inflate(inflater, container, false)
        return binding.root

    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        binding.list.layoutManager = LinearLayoutManager(context)
        binding.list.adapter = adapter

        viewModel.load(adapter, args.strategy)
    }

    private fun navigateToDetail(movie: Movie) {
        val directions =
            MovieListFragmentDirections.actionToMovieDetail(
                movie.id.toLong(),
                movie.title
            )
        findNavController().navigate(directions)
    }


    private fun navigateToSearch() {
        val directions =
            MovieListFragmentDirections.actionToSearch()
        findNavController().navigate(directions)
    }

    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        super.onCreateOptionsMenu(menu, inflater)
        inflater.inflate(R.menu.menu_base, menu)

    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        return when (item.itemId) {
            R.id.list_search -> {
                // navigate to search screen
                navigateToSearch()
                true
            }
            else -> {
                super.onOptionsItemSelected(item)
            }
        }
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }
}
