package cz.cvut.fit.and.vizeldim.movieapp.features.movie.sharecode.qr

import com.google.zxing.common.BitMatrix
import cz.cvut.fit.and.vizeldim.movieapp.features.movie.sharecode.CodeInterface

class ZxingQrCode(private val matrix: BitMatrix) : CodeInterface {
    override fun get(x: Int, y: Int): Boolean = matrix.get(x, y)

    override val width: Int
        get() = matrix.width

    override val height: Int
        get() = matrix.height
}