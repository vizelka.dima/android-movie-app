package cz.cvut.fit.and.vizeldim.movieapp.features.movie.data.retrofit

import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass

@JsonClass(generateAdapter = true)
data class ApiPerson(
    val id: Int,
    val name: String,
    @Json(name = "profile_path") val img: String?,
    @Json(name = "known_for_department") val knownForDepartment: String?
)

@JsonClass(generateAdapter = true)
class ApiPersonListResponse(
    val results: List<ApiPerson>
)
