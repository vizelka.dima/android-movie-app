package cz.cvut.fit.and.vizeldim.movieapp.features.movie.data.retrofit

import retrofit2.http.GET
import retrofit2.http.Path
import retrofit2.http.Query

interface MovieApiDescription {
    @GET("movie/{id}")
    suspend fun getById(
        @Path("id") id: Long,
        @Query("append_to_response") appendToResponse: String = "videos,credits"
    ): ApiMovie


    @GET("movie/popular")
    suspend fun getPopularPage(@Query("page") page: Int = 1): ApiMovieListResponse

    @GET("movie/top_rated")
    suspend fun getTopRatedPage(@Query("page") page: Int = 1): ApiMovieListResponse

    @GET("movie/upcoming")
    suspend fun getUpcomingPage(@Query("page") page: Int = 1): ApiMovieListResponse

    @GET("search/movie")
    suspend fun getPageByName(
        @Query("query") name: String,
        @Query("page") page: Int = 1
    ): ApiMovieListResponse


    @GET("discover/movie")
    suspend fun getPageByPersonId(
        @Query("with_cast") id: Long,
        @Query("page") page: Int = 1
    ): ApiMovieListResponse

    @GET("person/popular")
    suspend fun getPopularPersonPage(@Query("page") page: Int = 1): ApiPersonListResponse

    @GET("search/person")
    suspend fun getPersonPageByName(
        @Query("query") name: String,
        @Query("page") page: Int = 1
    ): ApiPersonListResponse

}