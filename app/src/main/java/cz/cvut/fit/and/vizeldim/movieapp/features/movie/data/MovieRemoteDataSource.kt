package cz.cvut.fit.and.vizeldim.movieapp.features.movie.data

import cz.cvut.fit.and.vizeldim.movieapp.features.movie.domain.Movie
import cz.cvut.fit.and.vizeldim.movieapp.features.movie.domain.Person

interface MovieRemoteDataSource {
    suspend fun getPopularPage(page: Int = 1): List<Movie>

    suspend fun getTopRatedPage(page: Int = 1): List<Movie>

    suspend fun getUpcomingPage(page: Int = 1): List<Movie>

    suspend fun getPageByName(
        name: String,
        page: Int = 1
    ): List<Movie>

    suspend fun getById(id: Long): Movie

    suspend fun getPopularPersonPage(page: Int = 1): List<Person>

    suspend fun getPersonPageByName(
        name: String,
        page: Int = 1
    ): List<Person>

    suspend fun getPageByPersonId(
        id: Long,
        page: Int = 1
    ): List<Movie>
}