package cz.cvut.fit.and.vizeldim.movieapp.features.movie.sharecode

interface CodeInterface {
    fun get(x: Int, y: Int): Boolean

    val width: Int

    val height: Int
}