package cz.cvut.fit.and.vizeldim.movieapp.features.movie.presentation.adapter.movie

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.core.view.isVisible
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import cz.cvut.fit.and.vizeldim.movieapp.databinding.ItemMovieBinding
import cz.cvut.fit.and.vizeldim.movieapp.features.movie.domain.SimpleMovie

class SimpleMovieAdapter(
    private val onClickHandler: (SimpleMovie) -> Unit
) :
    ListAdapter<SimpleMovie, SimpleMovieAdapter.CollectionHolder>(SimpleMovieDiffCallback()) {

    init {
        stateRestorationPolicy = StateRestorationPolicy.PREVENT_WHEN_EMPTY
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): CollectionHolder {
        val binding = ItemMovieBinding.inflate(
            LayoutInflater.from(parent.context),
            parent,
            false
        )

        return CollectionHolder(binding)
    }

    override fun onBindViewHolder(holder: CollectionHolder, position: Int) {
        val movieCollection = getItem(position)
        return holder.bind(movieCollection, onClickHandler)
    }

    class CollectionHolder(private val binding: ItemMovieBinding) :
        RecyclerView.ViewHolder(binding.root) {

        fun bind(
            movie: SimpleMovie,
            onClickHandler: (SimpleMovie) -> Unit
        ) {

            movie.img?.let {
                Glide.with(itemView).load("https://image.tmdb.org/t/p/w400${it}").into(binding.img)
            }

            binding.rating.root.isVisible = false
            binding.txtDate.isVisible = false
            binding.txtTitle.text = movie.title
            binding.root.setOnClickListener {
                onClickHandler(movie)
            }
        }
    }

    class SimpleMovieDiffCallback : DiffUtil.ItemCallback<SimpleMovie>() {
        override fun areItemsTheSame(oldItem: SimpleMovie, newItem: SimpleMovie): Boolean {
            return oldItem.id == newItem.id
        }

        override fun areContentsTheSame(
            oldItem: SimpleMovie,
            newItem: SimpleMovie
        ): Boolean {
            return oldItem == newItem
        }
    }

}