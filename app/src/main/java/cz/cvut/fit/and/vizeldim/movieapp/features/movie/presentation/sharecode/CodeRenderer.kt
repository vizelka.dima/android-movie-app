package cz.cvut.fit.and.vizeldim.movieapp.features.movie.presentation.sharecode

import android.graphics.Bitmap
import cz.cvut.fit.and.vizeldim.movieapp.features.movie.sharecode.CodeInterface

interface CodeRenderer {
    suspend fun render(rawData: CodeInterface): Bitmap
}