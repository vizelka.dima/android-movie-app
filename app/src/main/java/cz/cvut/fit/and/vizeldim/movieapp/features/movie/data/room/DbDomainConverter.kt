package cz.cvut.fit.and.vizeldim.movieapp.features.movie.data.room

import cz.cvut.fit.and.vizeldim.movieapp.features.movie.domain.MovieCollection
import cz.cvut.fit.and.vizeldim.movieapp.features.movie.domain.SimpleMovie
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.map

fun DbMovieCollection.toDomain(): MovieCollection =
    MovieCollection(collectionId, name)

@JvmName("toDomainDbMovieCollection")
fun Flow<DbMovieCollection>.toDomain(): Flow<MovieCollection> =
    map { it.toDomain() }


fun DbMovie.toDomain(): SimpleMovie =
    SimpleMovie(movieId, title, img)


fun Flow<List<DbMovieCollection>>.toDomain(): Flow<List<MovieCollection>> =
    map { it.toDomain() }

fun List<DbMovieCollection>.toDomain(): List<MovieCollection> =
    map { it.toDomain() }


@JvmName("toDomainDbMovieCollectionWithMovies")
fun Flow<DbMovieCollectionWithMovies>.toDomain(): Flow<List<SimpleMovie>> =
    map { it.movies.toDomain() }


@JvmName("toDomainDbMovie")
fun List<DbMovie>.toDomain(): List<SimpleMovie> =
    map { it.toDomain() }

