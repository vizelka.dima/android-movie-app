package cz.cvut.fit.and.vizeldim.movieapp.features.movie.presentation.viewmodel.movie

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import cz.cvut.fit.and.vizeldim.movieapp.features.movie.data.IMovieRepository
import cz.cvut.fit.and.vizeldim.movieapp.features.movie.domain.Movie
import kotlinx.coroutines.launch

class DetailViewModel(
    private val movieRepository: IMovieRepository
) : ViewModel() {
    private val _detailStateStream: MutableLiveData<DetailState> =
        MutableLiveData(DetailState())

    val detailStateStream: LiveData<DetailState>
        get() = _detailStateStream


    fun getById(id: Long) {
        if (_detailStateStream.value?.movieDetail == null) {
            _detailStateStream.value = _detailStateStream.value?.copy(isLoading = true)

            viewModelScope.launch {
                try {
                    movieRepository.getById(id).let {
                        _detailStateStream.value =
                            _detailStateStream.value?.copy(movieDetail = it)
                        _detailStateStream.value = _detailStateStream.value?.copy(isLoading = false)
                    }
                } catch (t: Throwable) {
                    _detailStateStream.value =
                        _detailStateStream.value?.copy(movieDetail = null, showError = true)
                } finally {
                    _detailStateStream.value = _detailStateStream.value?.copy(isLoading = false)
                }
            }
        }
    }


    fun hideError() {
        _detailStateStream.value = _detailStateStream.value?.copy(showError = false)
    }
}

data class DetailState(
    val movieDetail: Movie? = null,
    val isLoading: Boolean = false,
    val showError: Boolean = false)
