package cz.cvut.fit.and.vizeldim.movieapp.features.movie.data.paging.person

import cz.cvut.fit.and.vizeldim.movieapp.features.movie.data.IMovieRepository
import cz.cvut.fit.and.vizeldim.movieapp.features.movie.data.paging.APagingSource
import cz.cvut.fit.and.vizeldim.movieapp.features.movie.domain.Person

class PersonPagingSource(
    private val repository: IMovieRepository
) : APagingSource<Person>() {

    override suspend fun getPageData(page: Int): List<Person> =
        repository.getPopularPersonPage(page)
}