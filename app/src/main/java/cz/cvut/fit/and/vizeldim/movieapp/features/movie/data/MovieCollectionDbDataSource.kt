package cz.cvut.fit.and.vizeldim.movieapp.features.movie.data

import cz.cvut.fit.and.vizeldim.movieapp.features.movie.domain.MovieCollection
import cz.cvut.fit.and.vizeldim.movieapp.features.movie.domain.SimpleMovie
import kotlinx.coroutines.flow.Flow

interface MovieCollectionDbDataSource {
    fun streamAll(): Flow<List<MovieCollection>>

    suspend fun create(name: String): Long

    suspend fun create(name: String, movies: List<SimpleMovie>): Long

    suspend fun remove(collectionId: Long)

    suspend fun addMovie(collectionId: Long, movie: SimpleMovie): Long

    suspend fun removeMovie(collectionId: Long, movie: SimpleMovie): Boolean

    suspend fun updateCollectionName(collectionId: Long, name: String): Boolean

    fun streamMovies(collectionId: Long): Flow<List<SimpleMovie>>

    fun streamCollectionById(collectionId: Long): Flow<MovieCollection>
}