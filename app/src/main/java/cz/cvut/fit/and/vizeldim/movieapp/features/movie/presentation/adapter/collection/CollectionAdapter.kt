package cz.cvut.fit.and.vizeldim.movieapp.features.movie.presentation.adapter.collection

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import cz.cvut.fit.and.vizeldim.movieapp.databinding.ItemMovieCollectionBinding
import cz.cvut.fit.and.vizeldim.movieapp.features.movie.domain.MovieCollection

class CollectionAdapter(
    private val onClickHandler: (MovieCollection) -> Unit
) :
    ListAdapter<MovieCollection, CollectionAdapter.CollectionHolder>(CollectionDiffCallback()) {

    init {
        stateRestorationPolicy = StateRestorationPolicy.PREVENT_WHEN_EMPTY
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): CollectionHolder {
        val binding = ItemMovieCollectionBinding.inflate(
            LayoutInflater.from(parent.context),
            parent,
            false
        )

        return CollectionHolder(binding)
    }

    override fun onBindViewHolder(holder: CollectionHolder, position: Int) {
        val movieCollection = getItem(position)
        return holder.bind(movieCollection, onClickHandler)
    }

    class CollectionHolder(
        private val binding: ItemMovieCollectionBinding
    ) : RecyclerView.ViewHolder(binding.root) {
        fun bind(
            movieCollection: MovieCollection,
            onClickHandler: (MovieCollection) -> Unit
        ) {

            binding.txtCollectionName.text = movieCollection.name
            binding.root.setOnClickListener {
                onClickHandler(movieCollection)
            }
        }
    }

    class CollectionDiffCallback : DiffUtil.ItemCallback<MovieCollection>() {
        override fun areItemsTheSame(oldItem: MovieCollection, newItem: MovieCollection): Boolean {
            return oldItem.id == newItem.id
        }

        override fun areContentsTheSame(
            oldItem: MovieCollection,
            newItem: MovieCollection
        ): Boolean {
            return oldItem == newItem
        }

    }

}