package cz.cvut.fit.and.vizeldim.movieapp.features.movie.presentation.viewmodel.movie

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import cz.cvut.fit.and.vizeldim.movieapp.features.movie.data.IMovieRepository
import cz.cvut.fit.and.vizeldim.movieapp.features.movie.domain.Movie
import kotlinx.coroutines.launch

class SearchMovieViewModel(
    private val movieRepository: IMovieRepository
) : ViewModel() {
    private val _searchStateStream: MutableLiveData<SearchMovieState> = MutableLiveData(
        SearchMovieState()
    )
    val searchStateStream: LiveData<SearchMovieState> = _searchStateStream


    fun searchByName(name: String, movieFilter: ((Movie) -> Boolean)? = null) {
            if(name.isBlank()) {
                _searchStateStream.value =
                    _searchStateStream.value?.copy(searchName = name, foundMovies = emptyList())
            } else {
                _searchStateStream.value =
                    _searchStateStream.value?.copy(isLoading = true, searchName = name)

                viewModelScope.launch {
                    try {
                        val moviesResp =  movieRepository.getPageByName(name)
                        val movies = if(movieFilter == null) moviesResp else moviesResp.filter(movieFilter)

                        _searchStateStream.value = _searchStateStream.value?.copy(
                            foundMovies = movies,
                            searchName = name
                        )
                    } catch (t: Throwable) {
                        _searchStateStream.value = _searchStateStream.value?.copy(
                            foundMovies = emptyList(),
                            showError = true
                        )
                    } finally {
                        _searchStateStream.value = _searchStateStream.value?.copy(isLoading = false)
                    }
                }
        }
    }

    fun hideError() {
        _searchStateStream.value = _searchStateStream.value?.copy(showError = false)
    }

    fun clearFoundResults() {
        _searchStateStream.value =
            _searchStateStream.value?.copy(foundMovies = emptyList(), searchName = "")
    }


}


data class SearchMovieState(
    val foundMovies: List<Movie> = emptyList(),
    val searchName: String = "",
    val isLoading: Boolean = false,
    val showError: Boolean = false
)
