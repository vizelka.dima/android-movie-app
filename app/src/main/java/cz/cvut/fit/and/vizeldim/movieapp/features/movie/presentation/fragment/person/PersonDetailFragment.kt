package cz.cvut.fit.and.vizeldim.movieapp.features.movie.presentation.fragment.person

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.navigation.fragment.findNavController
import androidx.navigation.fragment.navArgs
import androidx.recyclerview.widget.LinearLayoutManager
import cz.cvut.fit.and.vizeldim.movieapp.databinding.FragmentPersonDetailBinding
import cz.cvut.fit.and.vizeldim.movieapp.features.movie.data.paging.movie.PersonMoviePagingStrategy
import cz.cvut.fit.and.vizeldim.movieapp.features.movie.domain.Movie
import cz.cvut.fit.and.vizeldim.movieapp.features.movie.presentation.adapter.movie.MoviePagingAdapter
import cz.cvut.fit.and.vizeldim.movieapp.features.movie.presentation.viewmodel.movie.MovieListViewModel
import org.koin.androidx.viewmodel.ext.android.viewModel

class PersonDetailFragment : Fragment() {
    private val args: PersonDetailFragmentArgs by navArgs()

    private var _binding: FragmentPersonDetailBinding? = null

    // This property is only valid between onCreateView and
    // onDestroyView.
    private val binding get() = _binding!!

    private val adapter = MoviePagingAdapter(::navigateToDetail)

    private val viewModel by viewModel<MovieListViewModel>()


    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        _binding = FragmentPersonDetailBinding.inflate(inflater, container, false)
        return binding.root
    }


    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        binding.list.layoutManager = LinearLayoutManager(context)
        binding.list.adapter = adapter


        viewModel.load(adapter, PersonMoviePagingStrategy(args.personId))

    }

    private fun navigateToDetail(movie: Movie) {
        val directions =
            PersonDetailFragmentDirections.actionToMovieDetail(
                movie.id.toLong(),
                movie.title
            )
        findNavController().navigate(directions)
    }
}